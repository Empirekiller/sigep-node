/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ErrorLogKey]
      ,[ErrorTimeStamp]
      ,[ErrorCode]
      ,[User]
  FROM [SWDB].[dbo].[ErrorLog]

  USE [SWDB]
GO
/****** Object:  StoredProcedure [SWS].[sp_getStudents]    Script Date: 04-01-2020 18:58:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_getStudents]
AS
BEGIN
    SELECT user_number, name, address, email from sws.users where user_types_id_type = 1
END

GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_getAdvisors]
AS
BEGIN
    SELECT user_number, name, address, email from sws.users where user_types_id_type = 2
END

-- Verifica se existe um email registado
GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_check_email] (@email varchar(45))
AS
BEGIN
    SELECT user_number, name, address, email from sws.users
END


-- Inquérito

GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_count_survey_answers] 
AS
BEGIN
	DECLARE @id INT
	EXECUTE [SWS].[sp_get_active_surveyid] @id OUTPUT 
	SELECT SUM(CASE WHEN answer = 1 THEN 1 ELSE 0 END) AS Interships,
	 SUM(CASE WHEN answer = 0 THEN 1 ELSE 0 END) AS Projects
	  FROM sws.survey_answers 
	where survey_idsurvey = @id
END

EXEC [SWS].[sp_count_survey_answers] 

-- 
GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_get_active_surveyid](@id int output)
AS
BEGIN
SEt @id =(SELECT distinct idsurvey AS surveyId FROM sws.survey where end_date >= getdate())

END

GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_get_active_survey]
AS
BEGIN
SELECT distinct idsurvey AS surveyId FROM sws.survey where end_date >= getdate()

END

GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_get_active_survey_answers]
AS
BEGIN
DECLARE @id INT
EXECUTE [SWS].[sp_get_active_surveyid] @id OUTPUT 
SELECT [users_user_number]
      ,[survey_idsurvey]
      ,[answer]
  FROM [SWS].[survey_answers]
  where [survey_idsurvey] = @id

END

GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_get_active_survey_user_answer](@userid int)
AS
BEGIN
DECLARE @id INT
EXECUTE [SWS].[sp_get_active_surveyid] @id OUTPUT 
SELECT [users_user_number]
      ,[survey_idsurvey]
      ,[answer]
  FROM [SWS].[survey_answers]
  where [survey_idsurvey] = @id and [users_user_number] = @userid;

END
--------------------------
GO
CREATE OR ALTER   PROCEDURE [SWS].[sp_count_students] 
AS
BEGIN
	select COUNT(distinct user_number) as Students from SWS.users where user_types_id_type = 1
END

select * from SWS.survey
select * from SWS.survey_answers
delete from SWS.survey;
delete from SWS.survey_answers;



/****** Object:  StoredProcedure [SWS].[sp_survey_insert]    Script Date: 05/01/2020 19:32:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		ALTER PROCEDURE [SWS].[sp_survey_insert]
		(@description varchar(45), @end_date date)
			AS
			BEGIN TRY
				INSERT INTO [SWS].[survey] ( [description], [start_date], [end_date])
				VALUES (@description,getdate(), @end_date)
			END TRY
			BEGIN CATCH
				DECLARE @ErrorNumber INT
				DECLARE @Line INT
			
				SELECT @ErrorNumber = ERROR_NUMBER()
				SELECT @Line = ERROR_LINE()

				EXEC [sp_ErrorLogMessage] @ErrorNumber,@Line
			END CATCH
		