create procedure SWS.sp_does_password_exists(@password varchar)
as begin
Declare @existsPassword varchar
SELECT @existsPassword = [SWS].[users].[password] from [SWS].[users] where [users].[password] = CAST(@password AS char(128));
if(@existsPassword = null)
return 0;
else
return 1;
END 
Go