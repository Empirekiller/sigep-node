﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using SIGEP.Controllers;
using SIGEP.Data;
using SIGEP.models;
using SIGEP.Models;
using SIGEPTests1.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace testSIGEP
{
    public class UserControllerTest
    {
        [Fact]
        public void StudentsTest()
        {
            var connection = new SqlConnection("Data Source=HENOCH-PC;Initial Catalog =SWDB; Integrated Security=True; Password = Password");
            connection.Open();
            var options = new DbContextOptionsBuilder<SWDBContext>()
                .UseSqlServer(connection)
                .Options;

            using (var context = new SWDBContext(options))
            {
                var controller = new UsersController(context, null);
                var result = controller.StudentsAsync() as Task<IActionResult>;
                var viewresult = result.Result as ViewResult;
                var model = viewresult.Model;
                Assert.NotNull(result);
                Assert.NotNull(model); // add additional checks on the Model
                Assert.True(string.IsNullOrEmpty(viewresult.ViewName) || viewresult.ViewName == "Students");
            }
        }

        [Fact]
        public void AdvisorsTest()
        {
            var connection = new SqlConnection("Data Source=HENOCH-PC;Initial Catalog =SWDB; Integrated Security=True; Password = Password");
            connection.Open();
            var options = new DbContextOptionsBuilder<SWDBContext>()
                .UseSqlServer(connection)
                .Options;
            using (var context = new SWDBContext(options))
            {
                var controller = new UsersController(context, null);
                var result = controller.AdvisorsAsync();
                var viewResult = Assert.IsType<ViewResult>(result);
            }
        }

        [Fact]
        public void Perfil()
        {
            var connection = new SqlConnection("Data Source=HENOCH-PC;Initial Catalog =SWDB; Integrated Security=True; Password = Password");
            connection.Open();
            var options = new DbContextOptionsBuilder<SWDBContext>()
                .UseSqlServer(connection)
                .Options;
            using (var context = new SWDBContext(options))
            {
                Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
                MockHttpSession mockSession = new MockHttpSession();
                mockSession["_Email"] = "170221014@estudantes.ips.pt";//has to exists in db
                mockSession["_Password"] = "6rgs%Xy32z";
                mockHttpContext.Setup(s => s.Session).Returns(mockSession);
                UsersController controller = new UsersController(context, null);
                controller.ControllerContext.HttpContext = mockHttpContext.Object;
                var controlador = controller;
                var result = controlador.PerfilAsync();
                Assert.IsType<ViewResult>(result);
            }
        }
    }
}
