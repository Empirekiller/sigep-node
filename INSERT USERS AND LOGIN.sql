USE [SWDB]
GO
/****** Object:  StoredProcedure [SWS].[sp_users_insert]    Script Date: 04-01-2020 17:44:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE OR ALTER PROCEDURE [SWS].[sp_users_insert]
		(@user_number int, @name varchar(45), @user_types_id_type int, @password varchar(128), @address varchar(45), @email varchar(45))
			AS
			BEGIN TRY
				INSERT INTO [SWS].[users] ([user_number], [name], [user_types_id_type], [password], [address], [email])
				VALUES (@user_number, @name, @user_types_id_type, CAST(@password as CHAR(128)), @address, @email)
			END TRY
			BEGIN CATCH
				DECLARE @ErrorNumber INT
				DECLARE @Line INT
			
				SELECT @ErrorNumber = ERROR_NUMBER()
				SELECT @Line = ERROR_LINE()

				EXEC [sp_ErrorLogMessage] @ErrorNumber,@Line
			END CATCH
		

GO
		USE [SWDB]
GO
/****** Object:  StoredProcedure [SWS].[sp_login]    Script Date: 04-01-2020 17:47:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE OR ALTER  PROCEDURE [SWS].[sp_login]
        ( @email varchar(45) , @password varchar(128))
            AS
            select user_number, name, address, email, user_types_id_type from sws.users where email = @email and password = @password