var kue = require("kue");
var Queue = kue.createQueue({
  prefix: 'q',
  redis: {//heroku
    port: 1234,
    host: '10.0.50.20',
    auth: 'password',
    db: 3, // if provided select a non-default redis db
    options: {
      // see https://github.com/mranney/node_redis#rediscreateclient
    }
  }
});

let scheduleJob = data => {
  Queue.createJob(
    data.jobName, 
    data.origin_user_name,
    data.receiver_user_name, 
    data.receiver_user_number,
    data.notification_format_text)
    .attempts(10)
    .delay('*/* 30 * * * *')//30 mins
    .save();
};

module.exports = {
  scheduleJob
};