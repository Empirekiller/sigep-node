"use strict";
const mssql = require("mssql/msnodesqlv8");
const options = require("./connection-options.json");
var nodemailer = require("nodemailer");
var validator = require("email-validator");
//const kue = require('./kue');
//require('./worker');
var Hogan = require("hogan.js");
var fs = require('fs');

//getfile
var template = fs.readFileSync('scripts/email-template.hjs', 'utf-8');
//compile template
var compiledTemplate = Hogan.compile(template);

function testSelect(req, res) {
    let sql_user = "SELECT * FROM [SWS].[users] WHERE [SWS].[users].[user_number] = @user_number";

    mssql.connect(options,function (err) {
      if (err) throw err;
      var request3 = new mssql.Request();
      request3.input('user_number', 180221090);

      request3.query(sql_user, function (err, rows) {
          if (err) {
              res.sendStatus(500);
              console.log(err);
          } else {
            
            /*console.log( rows);
            console.log(rows.recordset);
            console.log(rows.rowsAffected);*/
            console.log("the user is", rows.recordset[0]);

          } 
      });
  });
}
module.exports.testSelect = testSelect;
/**
 * Function to send e-mail of notification to a user.
 * @param {*} req 
 * @param {*} res 
 */
function sendNotificationEmail(req, res) {

  /*body format:

    {
      "origin_user_number":  180221090,
      "origin_user_name":  "Nicole Fernandes",
      "receiver_user_number": 170221014,
      "receiver_user_name": "Henoch Vitureira",
      "notification_format_id": 1,
      "notification_format_text": "A sua Proposta de estágio foi aceite"
  }

  */

    let origin_user_number = req.body.origin_user_number;
    let origin_user_name = req.body.origin_user_name;
    let receiver_user_number = req.body.receiver_user_number;
    let receiver_user_name = req.body.receiver_user_name;
    let notification_format_id = req.body.notification_format_id;
    let notification_format_text = req.body.notification_format_text;


    let sqlquery = `INSERT INTO [SWS].[notifications]
    ([SWS].[notifications].[id_notification_format],
    [SWS].[notifications].[origin_user_number],
    [SWS].[notifications].[receiver_user_number],
    [SWS].[notifications].[date_time],
    [SWS].[notifications].[marked_read]) 
    VALUES (@notification_format_id, @origin_user_number, @receiver_user_number, GETDATE(), 0)`;

    mssql.connect(options,function (err) {
        if (err) throw err;
        var request1 = new mssql.Request();

        request1.input('origin_user_number', origin_user_number);
        request1.input('receiver_user_number', receiver_user_number);
        request1.input('notification_format_id', notification_format_id);



        request1.query(sqlquery, function (err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
              
              console.log("notification inserted");
			  var receiverEmail = receiver_user_number + "@estudantes.ips.pt";
			  if(validator.validate(receiverEmail)){
			  sendEmail(origin_user_name, receiver_user_name, receiver_user_number, notification_format_text);
              res.send("Origin User : " + origin_user_name 
              + "\nReceiver User: " + receiver_user_name 
              + "\nReceiver User Number: " + receiver_user_number
              + "\nReceiver Mail : " +  receiver_user_number + "@estudantes.ips.pt"
              + "\nNotificação: " + notification_format_text );
			  } else {
				res.sendStatus(400);
                console.log("Invalid email"+receiverEmail);
			  }
            } 
        });
    });
}
module.exports.sendNotificationEmail = sendNotificationEmail;


/**
 * Function to send e-mail to a user.
 * @param {*} notification_body - The text of the notification, got from the database
 */
function sendEmail(origin_user_name, receiver_user_name, receiver_user_number, notification_format_text) {

        var transporter = nodemailer.createTransport({
            service: 'gmail',
            type: "SMTP",
            host: "smtp.gmail.com",
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
              user: 'suggestions.nahda@gmail.com',
              pass: 'sigep2019'
            }
          });
          
          var mailOptions = {
            from: 'suggestions.nahda@gmail.com',
            to: receiver_user_number+'@estudantes.ips.pt',
            subject: "SIGEP Notification",
            attachments: [{
              filename: 'logoLP.png',
              path: './images/logoLP.png',
              cid: 'cid: unique@kreata.ee'
         }],
            html: compiledTemplate.render({origin_user_name: origin_user_name,
                                          receiver_user_name:receiver_user_name,
                                          notification_format_text:notification_format_text})
          };
          
          transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
		        	/*let args = {
			        jobName: "resendEmail",
              origin_user_name: origin_user_name, 
              receiver_user_name: receiver_user_name,
              receiver_user_number: receiver_user_number, 
              notification_format_text: notification_format_text
		      	  }
			        kue.scheduleJob(args);*/
              console.log(error);
            } else {
              console.log('Email sent');
              
            }
          });
}
module.exports.sendEmail = sendEmail;