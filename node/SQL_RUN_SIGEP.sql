-- MSSQL Script DB creation + tables
-- SW
-- 8/12/2019
-- Model: DB_SW    Version: 1.0

-- Kill db :)
use [master];
DROP DATABASE IF EXISTS SWDB;

-- Create Database 
CREATE Database SWDB;
go

USE SWDB;
go

DROP SCHEMA if Exists SWS 
go

CREATE SCHEMA SWS
go
 -- Start Schema Table Construction

-- -----------------------------------------------------
-- Table sws.user_types
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.user_types ;

CREATE TABLE  sws.user_types (
  id_type INT NOT NULL IDENTITY(1,1),
  type VARCHAR(45) NOT NULL,
  PRIMARY KEY (id_type))

-- -----------------------------------------------------
-- Table sws.users
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.users ;

CREATE TABLE  sws.users (
  user_number INT NOT NULL ,
  name VARCHAR(45) NOT NULL,
  user_types_id_type INT NOT NULL DEFAULT(1),
  password CHAR(128) NOT NULL,
  address VARCHAR(45) NOT NULL,
  email VARCHAR(45) NOT NULL,
  PRIMARY KEY (user_number),
  INDEX fk_users_user_types1_idx (user_types_id_type ASC),
  CONSTRAINT fk_users_user_types1
    FOREIGN KEY (user_types_id_type)
    REFERENCES sws.user_types (id_type)
    ON DELETE CASCADE
    ON UPDATE CASCADE)



-- -----------------------------------------------------
-- Table sws.hosting_entity
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.hosting_entity ;

CREATE TABLE  sws.hosting_entity (
  id_hosting_entity INT NOT NULL IDENTITY(1,1),
  name VARCHAR(20) NOT NULL,
  telephone INT NULL,
  email VARCHAR(45) NOT NULL,
  address VARCHAR(50) NOT NULL,
  user_rep INT NULL,
  PRIMARY KEY (id_hosting_entity),
  INDEX fk_hosting_entity_users1_idx (user_rep ASC),
  CONSTRAINT fk_hosting_entity_users1
    FOREIGN KEY (user_rep)
    REFERENCES sws.users (user_number)
    ON DELETE CASCADE
    ON UPDATE CASCADE)



-- -----------------------------------------------------
-- Table sws.activity_types
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.activity_types ;

CREATE TABLE  sws.activity_types (
  id_type INT NOT NULL IDENTITY(1,1),
  type VARCHAR(45) NOT NULL,
  PRIMARY KEY (id_type))



-- -----------------------------------------------------
-- Table sws.activities
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.activities ;

CREATE TABLE  sws.activities (
  id_activity INT NOT NULL IDENTITY(1,1),
  id_hosting_entity INT NOT NULL,
  id_activity_types INT NOT NULL,
  id_student INT NULL,
  title VARCHAR(45) NOT NULL,
  vacancy_limit INT NOT NULL,
  location VARCHAR(45) NULL,
  main_requirements VARCHAR(45) NOT NULL,
  confidentiality_requirements VARCHAR(45) NULL,
  used_techs VARCHAR(45) NOT NULL,
  observations VARCHAR(500) NULL,
  description VARCHAR(500) NOT NULL,
  objectives VARCHAR(300) NOT NULL,
  work_time TIME NULL,
  PRIMARY KEY (id_activity),
  INDEX fk_internships_hosting_entity1_idx (id_hosting_entity ASC),
  INDEX fk_activities_users1_idx (id_student ASC),
  INDEX fk_activities_activity_types1_idx (id_activity_types ASC),
  CONSTRAINT fk_internships_hosting_entity1
    FOREIGN KEY (id_hosting_entity)
    REFERENCES sws.hosting_entity (id_hosting_entity)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_activities_users1
    FOREIGN KEY (id_student)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_activities_activity_types1
    FOREIGN KEY (id_activity_types)
    REFERENCES sws.activity_types (id_type)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.document_types
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.document_types ;

CREATE TABLE  sws.document_types (
  id_document_type INT NOT NULL IDENTITY(1,1),
  type VARCHAR(45) NOT NULL,
  PRIMARY KEY (id_document_type))



-- -----------------------------------------------------
-- Table sws.activity_documents
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.activity_documents ;

CREATE TABLE  sws.activity_documents (
  activities_id_activity INT NOT NULL ,
  id_document_types INT NOT NULL,
  name VARCHAR(45) NOT NULL,
  date_of_upload DATE NOT NULL,
  path VARCHAR(150) NOT NULL,
  INDEX fk_activity_documents_activities1_idx (activities_id_activity ASC),
  INDEX fk_activity_documents_document_types1_idx (id_document_types ASC),
  PRIMARY KEY (activities_id_activity),
  CONSTRAINT fk_activity_documents_activities1
    FOREIGN KEY (activities_id_activity)
    REFERENCES sws.activities (id_activity)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT fk_activity_documents_document_types1
    FOREIGN KEY (id_document_types)
    REFERENCES sws.document_types (id_document_type)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.notification_format
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.notification_format ;

CREATE TABLE  sws.notification_format (
  id_notification_format INT NOT NULL IDENTITY(1,1),
  name VARCHAR(45) NOT NULL,
  text TEXT NOT NULL,
  PRIMARY KEY (id_notification_format))



-- -----------------------------------------------------
-- Table sws.notifications
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.notifications ;

CREATE TABLE  sws.notifications (
  id_notification INT NOT NULL IDENTITY(1,1),
  id_notification_format INT NOT NULL,
  origin_user_number INT NOT NULL,
  receiver_user_number INT NOT NULL,
  date_time DATETIME NOT NULL,
  marked_read bit NOT NULL DEFAULT 0,
  INDEX fk_notifications_notification_format1_idx (id_notification_format ASC),
  INDEX fk_notifications_users1_idx (origin_user_number ASC),
  INDEX fk_notifications_users2_idx (receiver_user_number ASC),
  PRIMARY KEY (id_notification),
  CONSTRAINT fk_notifications_notification_format1
    FOREIGN KEY (id_notification_format)
    REFERENCES sws.notification_format (id_notification_format)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_notifications_users1
    FOREIGN KEY (origin_user_number)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_notifications_users2
    FOREIGN KEY (receiver_user_number)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.survey
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.survey ;

CREATE TABLE  sws.survey (
  id_survey INT NOT NULL,
  description VARCHAR(45) NOT NULL DEFAULT 'Deseja realizar Est�gio ou Projeto?',
  start_date DATE NOT NULL DEFAULT GETDATE(),
  survey_end_date DATE NOT NULL DEFAULT DATEADD(DAY,1,GETDATE()),
  PRIMARY KEY (id_survey),
  CONSTRAINT CHK_SURVEY_DATES CHECK (survey_end_date > start_date) 
)


-- -----------------------------------------------------
-- Table sws.survey_answers
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.survey_answers ;

CREATE TABLE  sws.survey_answers (
  users_user_number INT NOT NULL,
  survey_id_survey INT NOT NULL,
  answer BIT NOT NULL,
  INDEX fk_survey_answers_users1_idx (users_user_number ASC),
  INDEX fk_survey_answers_survey1_idx (survey_id_survey ASC),
  PRIMARY KEY (survey_id_survey),
  CONSTRAINT fk_survey_answers_users1
    FOREIGN KEY (users_user_number)
    REFERENCES sws.users (user_number)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_survey_answers_survey1
    FOREIGN KEY (survey_id_survey)
    REFERENCES sws.survey (id_survey)
    ON DELETE CASCADE
    ON UPDATE CASCADE)

/****** Object:  Table [SWS].[survey_answers]    Script Date: 05/01/2020 19:10:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SWS].[survey_answers](
	[users_user_number] [int] NOT NULL,
	[survey_idsurvey] [int] NOT NULL,
	[answer] [bit] NULL,
 CONSTRAINT [PK_survey_answers] PRIMARY KEY CLUSTERED 
(
	[users_user_number] ASC,
	[survey_idsurvey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [SWS].[survey_answers]  WITH CHECK ADD  CONSTRAINT [fk_survey_answers_survey1] FOREIGN KEY([survey_idsurvey])
REFERENCES [SWS].[survey] ([idsurvey])
GO

ALTER TABLE [SWS].[survey_answers] CHECK CONSTRAINT [fk_survey_answers_survey1]
GO

ALTER TABLE [SWS].[survey_answers]  WITH CHECK ADD  CONSTRAINT [fk_survey_answers_users1] FOREIGN KEY([users_user_number])
REFERENCES [SWS].[users] ([user_number])
GO

ALTER TABLE [SWS].[survey_answers] CHECK CONSTRAINT [fk_survey_answers_users1]
GO





-- -----------------------------------------------------
-- Table sws.student_advisor
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.student_advisor ;

CREATE TABLE  sws.student_advisor (
  id_advisor INT NOT NULL,
  id_student INT NOT NULL,
  approved BIT NULL,
  PRIMARY KEY (id_advisor, id_student),
  INDEX fk_student_advisor_activity_users2_idx (id_advisor ASC),
  INDEX fk_student_advisor_activity_users1_idx (id_student ASC),
  CONSTRAINT fk_student_advisor_activity_users2
    FOREIGN KEY (id_advisor)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_student_advisor_activity_users1
    FOREIGN KEY (id_student)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.final_evaluation
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.final_evaluation ;

CREATE TABLE  sws.final_evaluation (
  id_evaluation INT NOT NULL IDENTITY(1,1),
  id_student INT NOT NULL,
  date_time DATETIME NOT NULL,
  room NVARCHAR(45) NOT NULL,
  grade INT NULL,
  record VARCHAR(260) NULL,--ata
  approved BIT NULL,
  INDEX fk_final_evaluation_users1_idx (id_student ASC),
  PRIMARY KEY (id_evaluation),
  CONSTRAINT fk_final_evaluation_users1
    FOREIGN KEY (id_student)
    REFERENCES sws.users (user_number)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)

-- -----------------------------------------------------
-- Table sws.evaluation_juri
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.evaluation_juri ;

CREATE TABLE  sws.evaluation_juri (
  id_juri_member INT NOT NULL,
  id_final_evaluation INT NOT NULL,
  INDEX fk_evaluation_juri_users1_idx (id_juri_member ASC),
  INDEX fk_evaluation_juri_final_evaluation1_idx (id_final_evaluation ASC),
  PRIMARY KEY (id_juri_member, id_final_evaluation),
  CONSTRAINT fk_evaluation_juri_users1
    FOREIGN KEY (id_juri_member)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_evaluation_juri_final_evaluation1
    FOREIGN KEY (id_final_evaluation)
    REFERENCES sws.final_evaluation (id_evaluation)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.activity_proposal
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.activity_proposal ;

CREATE TABLE  sws.activity_proposal (
  activities_id_activity INT NOT NULL,
  users_id_user INT NOT NULL,
  approved BIT NULL,
  INDEX fk_proposal_users1_idx (users_id_user ASC),
  INDEX fk_proposal_activities1_idx (activities_id_activity ASC),
  PRIMARY KEY (activities_id_activity),
  CONSTRAINT fk_proposal_users1
    FOREIGN KEY (users_id_user)
    REFERENCES sws.users (user_number)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT fk_proposal_activities1
    FOREIGN KEY (activities_id_activity)
    REFERENCES sws.activities (id_activity)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.proposal_candidate
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.proposal_candidate ;

CREATE TABLE  sws.proposal_candidate (
  id_activity INT NOT NULL,
  id_student INT NOT NULL,
  approved BIT NULL,
  INDEX fk_proposal_candidate_activity_proposal1_idx (id_activity ASC),
  INDEX fk_proposal_candidate_users1_idx (id_student ASC),
  PRIMARY KEY (id_activity, id_student),
  CONSTRAINT fk_proposal_candidate_activity_proposal1
    FOREIGN KEY (id_activity)
    REFERENCES sws.activity_proposal (activities_id_activity)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_proposal_candidate_users1
    FOREIGN KEY (id_student)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.evaluation_approval
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.evaluation_approval ;

CREATE TABLE  sws.evaluation_approval (
  id_participant INT NOT NULL,
  id_evaluation INT NOT NULL,
  approved BIT NULL,
  INDEX fk_evaluation_approval_users1_idx (id_participant ASC),
  INDEX fk_evaluation_approval_final_evaluation1_idx (id_evaluation ASC),
  PRIMARY KEY (id_participant, id_evaluation),
  CONSTRAINT fk_evaluation_approval_users1
    FOREIGN KEY (id_participant)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_evaluation_approval_final_evaluation1
    FOREIGN KEY (id_evaluation)
    REFERENCES sws.final_evaluation (id_evaluation)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.student_task
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.student_task ;

CREATE TABLE  sws.student_task (
  id_task INT NOT NULL IDENTITY(1,1),
  id_student INT NOT NULL,
  title VARCHAR(45) NOT NULL,
  start_date DATE NOT NULL DEFAULT GETDATE(),
  task_end_date DATE NULL  DEFAULT (DATEADD(DAY,1,GETDATE())),
  description TEXT NOT NULL,
  INDEX fk_student_task_users1_idx (id_student ASC),
  PRIMARY KEY (id_task),
  CONSTRAINT CHK_TASK_DATES CHECK (task_end_date > start_date),
  CONSTRAINT fk_student_task_users1
    FOREIGN KEY (id_student)
    REFERENCES sws.users (user_number)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table sws.message
-- -----------------------------------------------------
DROP TABLE IF EXISTS sws.message ;

CREATE TABLE  sws.message (
  id_message INT NOT NULL IDENTITY(1,1),
  title VARCHAR(45) NOT NULL,
  content TEXT NOT NULL,
  users_user_number INT NOT NULL,
  hosting_entity_id_hosting_entity INT NOT NULL,
  PRIMARY KEY (id_message),
  INDEX fk_message_users1_idx (users_user_number ASC),
  INDEX fk_message_hosting_entity1_idx (hosting_entity_id_hosting_entity ASC),
  CONSTRAINT fk_message_users1
    FOREIGN KEY (users_user_number)
    REFERENCES sws.users (user_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_message_hosting_entity1
    FOREIGN KEY (hosting_entity_id_hosting_entity)
    REFERENCES sws.hosting_entity (id_hosting_entity)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
	--- this makes sense
---------------------------------------------------
--INSERTS
---------------------------------
	
USE [SWDB]
GO

INSERT INTO [SWS].[user_types]
           ([type])
     VALUES
           ('Alunos'),
		   ('Respons�vel da UC'),
		   ('Docente Orientador'),
		   ('Admin')
GO

USE [SWDB]
GO

INSERT INTO [SWS].[notification_format]
           ([name]
           ,[text])
     VALUES
           ('Proposta de est�gio aceite'
           ,'A sua Proposta de est�gio foi aceite'),
		   ('Proposta de est�gio recusada'
           ,'A sua Proposta de est�gio foi recusada'),
		   		   ('Proposta de projeto aceite'
           ,'A sua Proposta de projeto foi aceite'),
		   		   ('Proposta de projeto recusada'
           ,'A sua Proposta de projeto foi recusada')
GO


USE [SWDB]
GO

INSERT INTO [SWS].[users]
           ([user_number]
           ,[name]
           ,[user_types_id_type]
           ,[password]
           ,[address]
           ,[email])
     VALUES
           (180221090, 'Nicole Fernandes'
           ,1
           , 1010100011101010
		   , 'Rua da Nicole'
           ,'nicolemarlenefernandes@gmail.com'),
		   (170221014, 'Henoch Vitureira'
           ,1
           ,1010100011101010
		   , 'Rua do Henoch'
           ,'henoch@gmail.com')
		   
GO



