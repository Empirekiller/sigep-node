﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SIGEP.models;

namespace SIGEP.ViewComponents
{
    public class DiaryTaskViewComponent : ViewComponent
    {
        private SWDBContext _context;

        public DiaryTaskViewComponent(SWDBContext context)
        {
            _context = context;
          
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            StudentTask task = new StudentTask();
            string email = HttpContext.Session.GetString("_Email");
            var user = _context.Users.Where(u => u.Email.Equals(email)).FirstOrDefault();
            ViewData["idStudent"] = user.UserNumber;
            return View(task);
        }
    }
}
