using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using SIGEP.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;
using SIGEP.models;


namespace SIGEP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Login Sessions
            services.AddMvc();
            services.AddDistributedMemoryCache();
          
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(900);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // END Login Sessions
            services.AddDbContext<SWDBContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddControllersWithViews();
            services.AddRazorPages();
            //sservices.AddScoped<IDiaryEntry, DiaryEntry>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSession();
            app.UseEndpoints(endpoints =>
            {

                endpoints.MapControllerRoute(
                  name: "HomePage",
                  pattern: "/Homepage",
                  defaults: new
                  {
                      controller = "Home",
                      action = "Homepage"
                  });
                endpoints.MapControllerRoute(
                   name: "Perfil",
                   pattern: "/Perfil",
                   defaults: new
                   {
                       controller = "Users",
                       action = "Perfil"
                   });

                endpoints.MapControllerRoute(
                   name: "Advisors",
                   pattern: "/Advisors",
                   defaults: new
                   {
                       controller = "Users",
                       action = "Advisors"
                   });
                endpoints.MapControllerRoute(
                    name: "Students",
                    pattern: "/Students",
                    defaults: new
                    {
                        controller = "Users",
                        action = "Students"
                    });
                endpoints.MapControllerRoute(
                    name: "Contacts",
                    pattern: "/Contacts",
                    defaults: new
                    {
                        controller = "Home",
                        action = "Contacts"
                    });
                endpoints.MapControllerRoute(
                    name: "Recover Password",
                    pattern: "/RecoverPassword",
                    defaults: new
                    {
                        controller = "Home",
                        action = "RecoverPassword"
                    });
                endpoints.MapControllerRoute(
                    name: "Login",
                    pattern: "/Login",
                    defaults: new
                    {
                        controller = "Home",
                        action = "Login"
                    });
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
