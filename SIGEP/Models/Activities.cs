﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SIGEP.models
{
    public partial class Activities
    {
        public int IdActivity { get; set; }

        [Display(Name = "Entidade")]
        public int IdHostingEntity { get; set; }

        [Display(Name = "Tipo")]
        public int IdActivityTypes { get; set; }
        public int? IdStudent { get; set; }

        [Display(Name = "Título")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Vagas")]
        [Required]

        public int VacancyLimit { get; set; }

        [Display(Name = "Localização")]
        [Required]

        public string Location { get; set; }

        [Display(Name = "Requisitos Principais")]
        [Required]

        public string MainRequirements { get; set; }

        [Display(Name = "Requisitos de Confidencialidade")]
        [Required]

        public string ConfidentialityRequirements { get; set; }

        [Display(Name = "Tecnologias Utilizadas")]
        [Required]

        public string UsedTechs { get; set; }

        [Display(Name = "Observações")]
        public string Observations { get; set; }

        [Display(Name = "Descrição")]
        [Required]

        public string Description { get; set; }

        [Display(Name = "Objetivos")]
        [Required]

        public string Objectives { get; set; }

        [Display(Name = "Horário de Trabalho")]
        [Required]

        public TimeSpan? WorkTime { get; set; }

        public virtual ActivityTypes IdActivityTypesNavigation { get; set; }
        public virtual HostingEntity IdHostingEntityNavigation { get; set; }

        [Display(Name = "Aluno")]
        public virtual Users IdStudentNavigation { get; set; }
        public virtual ActivityDocuments ActivityDocuments { get; set; }
        public virtual ActivityProposal ActivityProposal { get; set; }
    }
}
