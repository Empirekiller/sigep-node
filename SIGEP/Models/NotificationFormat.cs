﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class NotificationFormat
    {
        public NotificationFormat()
        {
            Notifications = new HashSet<Notifications>();
        }

        public int IdNotificationFormat { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }

        public virtual ICollection<Notifications> Notifications { get; set; }
    }
}
