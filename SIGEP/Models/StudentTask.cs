﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SIGEP.models
{
    public  class StudentTask
    {
        public int IdTask { get; set; }
        public int IdStudent { get; set; }
        public string Title { get; set; }
       
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? TaskEndDate { get; set; }
        public string Description { get; set; }
        public virtual Users IdStudentNavigation { get; set; }
    }
}
