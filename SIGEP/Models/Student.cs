﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIGEP.Models
{
    public class Student : User
    {
        public Student(int user_number, string name, string address, int user_type) : base(user_number, name, address, user_type)
        {

        }

        public Student(int user_number, string name, string address, string email, int user_type) : base(user_number, name, address, email, user_type)
        {
        }
    }
}
