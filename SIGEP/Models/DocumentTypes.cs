﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class DocumentTypes
    {
        public DocumentTypes()
        {
            ActivityDocuments = new HashSet<ActivityDocuments>();
        }

        public int IdDocumentType { get; set; }
        public string Type { get; set; }

        public virtual ICollection<ActivityDocuments> ActivityDocuments { get; set; }
    }
}
