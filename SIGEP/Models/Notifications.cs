﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class Notifications
    {
        public int IdNotification { get; set; }
        public int IdNotificationFormat { get; set; }
        public int OriginUserNumber { get; set; }
        public int ReceiverUserNumber { get; set; }
        public DateTime DateTime { get; set; }
        public bool MarkedRead { get; set; }

        public virtual NotificationFormat IdNotificationFormatNavigation { get; set; }
        public virtual Users OriginUserNumberNavigation { get; set; }
        public virtual Users ReceiverUserNumberNavigation { get; set; }
    }
}
