﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class SurveyAnswers
    {
        public int UsersUserNumber { get; set; }
        public int SurveyIdSurvey { get; set; }
        public bool Answer { get; set; }

        public virtual Survey SurveyIdSurveyNavigation { get; set; }
        public virtual Users UsersUserNumberNavigation { get; set; }
    }
}
