﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class ActivityDocuments
    {
        public int ActivitiesIdActivity { get; set; }
        public int IdDocumentTypes { get; set; }
        public string Name { get; set; }
        public DateTime DateOfUpload { get; set; }
        public string Path { get; set; }

        public virtual Activities ActivitiesIdActivityNavigation { get; set; }
        public virtual DocumentTypes IdDocumentTypesNavigation { get; set; }
    }
}
