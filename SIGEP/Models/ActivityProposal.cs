﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class ActivityProposal
    {
        public ActivityProposal()
        {
            ProposalCandidate = new HashSet<ProposalCandidate>();
        }

        public int ActivitiesIdActivity { get; set; }
        public int UsersIdUser { get; set; }
        public bool? Approved { get; set; }

        public virtual Activities ActivitiesIdActivityNavigation { get; set; }
        public virtual Users UsersIdUserNavigation { get; set; }
        public virtual ICollection<ProposalCandidate> ProposalCandidate { get; set; }
    }
}
