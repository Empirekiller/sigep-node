﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class UserTypes
    {
        public UserTypes()
        {
            Users = new HashSet<Users>();
        }

        public int IdType { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}
