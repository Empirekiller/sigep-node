﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class EvaluationJuri
    {
        public int IdJuriMember { get; set; }
        public int IdFinalEvaluation { get; set; }

        public virtual FinalEvaluation IdFinalEvaluationNavigation { get; set; }
        public virtual Users IdJuriMemberNavigation { get; set; }
    }
}
