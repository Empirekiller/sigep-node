﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public  class Users
    {
        
        public Users()
        {
            Activities = new HashSet<Activities>();
            ActivityProposal = new HashSet<ActivityProposal>();
            EvaluationApproval = new HashSet<EvaluationApproval>();
            EvaluationJuri = new HashSet<EvaluationJuri>();
            FinalEvaluation = new HashSet<FinalEvaluation>();
            HostingEntity = new HashSet<HostingEntity>();
            Message = new HashSet<Message>();
            NotificationsOriginUserNumberNavigation = new HashSet<Notifications>();
            NotificationsReceiverUserNumberNavigation = new HashSet<Notifications>();
            ProposalCandidate = new HashSet<ProposalCandidate>();
            StudentAdvisorIdAdvisorNavigation = new HashSet<StudentAdvisor>();
            StudentAdvisorIdStudentNavigation = new HashSet<StudentAdvisor>();
            StudentTask = new HashSet<StudentTask>();
            SurveyAnswers = new HashSet<SurveyAnswers>();
        }

        public int UserNumber { get; set; }
        public string Name { get; set; }
        public int UserTypesIdType { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public virtual UserTypes UserTypesIdTypeNavigation { get; set; }
        public virtual ICollection<Activities> Activities { get; set; }
        public virtual ICollection<ActivityProposal> ActivityProposal { get; set; }
        public virtual ICollection<EvaluationApproval> EvaluationApproval { get; set; }
        public virtual ICollection<EvaluationJuri> EvaluationJuri { get; set; }
        public virtual ICollection<FinalEvaluation> FinalEvaluation { get; set; }
        public virtual ICollection<HostingEntity> HostingEntity { get; set; }
        public virtual ICollection<Message> Message { get; set; }
        public virtual ICollection<Notifications> NotificationsOriginUserNumberNavigation { get; set; }
        public virtual ICollection<Notifications> NotificationsReceiverUserNumberNavigation { get; set; }
        public virtual ICollection<ProposalCandidate> ProposalCandidate { get; set; }
        public virtual ICollection<StudentAdvisor> StudentAdvisorIdAdvisorNavigation { get; set; }
        public virtual ICollection<StudentAdvisor> StudentAdvisorIdStudentNavigation { get; set; }
        public virtual ICollection<StudentTask> StudentTask { get; set; }
        public virtual ICollection<SurveyAnswers> SurveyAnswers { get; set; }


        public Users(int UserNumber, string Name, string Address, int UserTypesIdType)
        {
            this.UserNumber = UserNumber;
            this.Name = Name;
            this.Address = Address;
            this.Password = "hello";
            this.UserTypesIdType = UserTypesIdType;
            this.Email = construct_email(UserTypesIdType, UserNumber);
            Activities = new HashSet<Activities>();
            ActivityProposal = new HashSet<ActivityProposal>();
            EvaluationApproval = new HashSet<EvaluationApproval>();
            EvaluationJuri = new HashSet<EvaluationJuri>();
            FinalEvaluation = new HashSet<FinalEvaluation>();
            HostingEntity = new HashSet<HostingEntity>();
            Message = new HashSet<Message>();
            NotificationsOriginUserNumberNavigation = new HashSet<Notifications>();
            NotificationsReceiverUserNumberNavigation = new HashSet<Notifications>();
            ProposalCandidate = new HashSet<ProposalCandidate>();
            StudentAdvisorIdAdvisorNavigation = new HashSet<StudentAdvisor>();
            StudentAdvisorIdStudentNavigation = new HashSet<StudentAdvisor>();
            StudentTask = new HashSet<StudentTask>();
            SurveyAnswers = new HashSet<SurveyAnswers>();
        }
        private string construct_email(int UserTypesIdType, int UserNumber)
        {
            if (UserTypesIdType == 1)
                return UserNumber + "@estudantes.ips.pt";
            if (UserTypesIdType == 2)
                return UserNumber + "@estudantes.ips.pt";
            return null;
        }
    }
}
