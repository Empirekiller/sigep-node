﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class EvaluationApproval
    {
        public int IdParticipant { get; set; }
        public int IdEvaluation { get; set; }
        public bool? Approved { get; set; }

        public virtual FinalEvaluation IdEvaluationNavigation { get; set; }
        public virtual Users IdParticipantNavigation { get; set; }
    }
}
