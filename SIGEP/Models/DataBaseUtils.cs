﻿namespace SIGEP.Models
{
    using Microsoft.AspNetCore.Http;
    using OfficeOpenXml;
    using SIGEP.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using MailKit.Net.Smtp;
    using MimeKit;
    using System.Threading.Tasks;
    using SIGEP.models;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    public class DataBaseUtils
    {
        private readonly SWDBContext _context;

        public DataBaseUtils(SWDBContext context)
        {
            _context = context;
        }

        private async void InsertUsers(List<Users> imported_users)
        {
            try
            {
                 foreach (Users user in imported_users)
                 {
                   string passRandom = CheckPassToUser();
                  //JUST FOR NOT SENDING PASSWORD
                     SendEmailUserPassword(passRandom, user.Email);
                   string ecryptPass = encryptPasswordUser(passRandom);
                    user.Password = ecryptPass;
                    _context.Users.Add(user);
                 
                 }
                 _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                Console.WriteLine("could NOT connect to db! error: " + error);
            }
        }

        private  string CheckPassToUser()
        {
            string randomPass = "";
            do
            {
               randomPass = randomPassword();
            } while (GetPasswordsList(randomPass) == true);
            return randomPass;
        }
            
     
        private bool GetPasswordsList(string passToUser)
        {
            return _context.Users.Any(e => e.Password.Equals(passToUser));
        }

        private static string randomPassword()
        {
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
           return passwordString;
        }

        public static string encryptPasswordUser(string pass)
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes(pass);
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);
                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                var hashedInputStringBuilder = new System.Text.StringBuilder(128);
                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                return hashedInputStringBuilder.ToString();
            }
        }

        public async void import_users_excel(String filename)
        {
            //path to the cloned file(uploaded)
            FileInfo theSheet = new FileInfo(filename);
            var package = new ExcelPackage(theSheet);
            //list of users to add to the database
            List<Users> user_list = new List<Users>();
            ExcelWorksheet workSheet = package.Workbook.Worksheets[0];
            var start = workSheet.Dimension.Start;
            var end = workSheet.Dimension.End;
            for (int row = start.Row; row <= end.Row; row++)
            { // Row by row...
                object first_cell_val = workSheet.Cells[row, 1].Value;
                //check user number digitis
                if (first_cell_val == null || first_cell_val.ToString().Length < 9)
                    break;
                object second_cell_val = workSheet.Cells[row, 2].Value;
                //check user name digits
                if (second_cell_val == null || second_cell_val.ToString().Length < 4)
                    break;
                object third_cell_val = workSheet.Cells[row, 3].Value;
                //check user address digits
                if (third_cell_val == null || third_cell_val.ToString().Length < 6)
                    break;
                int user_number = Convert.ToInt32(workSheet.Cells[row, 1].Value);
                string user_name = workSheet.Cells[row, 2].Text;
                string user_address = workSheet.Cells[row, 3].Text;
                Users toAdd = new Users(user_number, user_name, user_address, 1);
                user_list.Add(toAdd);
            }
            InsertUsers(user_list);
            theSheet.Delete();
            package.Dispose();
        }

        public async void import_usersAdvisor_excel(String filename)
        {
            //path to the cloned file(uploaded)
            FileInfo theSheet = new FileInfo(filename);
            var package = new ExcelPackage(theSheet);
            //list of users to add to the database
            List<Users> user_list = new List<Users>();
            ExcelWorksheet workSheet = package.Workbook.Worksheets[0];
            var start = workSheet.Dimension.Start;
            var end = workSheet.Dimension.End;
            for (int row = start.Row; row <= end.Row; row++)
            { // Row by row...
                object first_cell_val = workSheet.Cells[row, 1].Value;
                //check user number digitis
                if (first_cell_val == null || first_cell_val.ToString().Length < 9)
                    break;
                object second_cell_val = workSheet.Cells[row, 2].Value;
                //check user name digits
                if (second_cell_val == null || second_cell_val.ToString().Length < 4)
                    break;
                object third_cell_val = workSheet.Cells[row, 3].Value;
                //check user address digits
                if (third_cell_val == null || third_cell_val.ToString().Length < 6)
                    break;
                int user_number = Convert.ToInt32(workSheet.Cells[row, 1].Value);
                string user_name = workSheet.Cells[row, 2].Text;
                string user_address = workSheet.Cells[row, 3].Text;
                Users toAdd = new Users(user_number, user_name, user_address, 2);
                user_list.Add(toAdd);
            }
            InsertUsers(user_list);
            theSheet.Delete();
            package.Dispose();
        }

        private static void SendEmailUserPassword(string password, string email)
        {
            try
            {
                MimeMessage message = new MimeMessage();
                MailboxAddress from = new MailboxAddress("Admin",
                "suggestions.nahda@gmail.com");
                message.From.Add(from);
                MailboxAddress to = new MailboxAddress("User", email);
                message.To.Add(to);
                message.Subject = "SIGEP - A sua password";
                BodyBuilder bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = "<h1>A sua password é: " + password + "</h1>";
                bodyBuilder.TextBody = "A sua password é " + password;
                message.Body = bodyBuilder.ToMessageBody();
                SmtpClient client = new SmtpClient();
                client.Connect("smtp.gmail.com", 465, true);
                client.Authenticate("suggestions.nahda@gmail.com", "sigep2019");
                client.Send(message);
                client.Disconnect(true);
                client.Dispose();
            }catch(Exception ex)
            {
                Console.WriteLine("ERRO : " + ex);
            }
           
        }



        public static void SendEmailRecoveryPassword(string url, string email)
        {
            try
            {
                MimeMessage message = new MimeMessage();
                MailboxAddress from = new MailboxAddress("Admin",
                "suggestions.nahda@gmail.com");
                message.From.Add(from);
                MailboxAddress to = new MailboxAddress("User", email);
                message.To.Add(to);
                message.Subject = "SIGEP - Recuperação da sua password";
                BodyBuilder bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = "<h1>Abra este link para renovar a sua password " + url +" </h1>";
                bodyBuilder.TextBody = "Abra este link para renovar a sua password " + url;
                message.Body = bodyBuilder.ToMessageBody();
                SmtpClient client = new SmtpClient();
                client.Connect("smtp.gmail.com", 465, true);
                client.Authenticate("suggestions.nahda@gmail.com", "sigep2019");
                client.Send(message);
                client.Disconnect(true);
                client.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERRO : " + ex);
            }

        }


    }
}
