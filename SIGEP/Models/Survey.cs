﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class Survey
    {

        public int IdSurvey { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime SurveyEndDate { get; set; }

        public virtual SurveyAnswers SurveyAnswers { get; set; }

    }
}
