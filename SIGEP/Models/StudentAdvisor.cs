﻿using System;
using System.Collections.Generic;

namespace SIGEP.models
{
    public partial class StudentAdvisor
    {
        public int IdAdvisor { get; set; }
        public int IdStudent { get; set; }
        public bool? Approved { get; set; }

        public virtual Users IdAdvisorNavigation { get; set; }
        public virtual Users IdStudentNavigation { get; set; }
    }
}
