﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIGEP.Data;
using SIGEP.models;
using SIGEP.Models;

namespace SIGEP.Controllers
{
    public class UsersController : Controller
    {
        private readonly SWDBContext _context;
        private readonly IWebHostEnvironment _env;
        public UsersController(SWDBContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }
        public async Task<IActionResult> StudentsAsync()
        {
            //ViewBag.survey = (Survey)_context.Survey.Where(b => b.SurveyEndDate < DateTime.Now);
            return View(await _context.Users
                .Where(u => u.UserTypesIdType == 1)
                .ToListAsync());
        }

        public async Task<IActionResult> AdvisorsAsync()
        {
            return View(await _context.Users
                .Where(u => u.UserTypesIdType == 2)
                .ToListAsync());
        }

        public async Task<IActionResult> PerfilAsync()
        {
            string email = HttpContext.Session.GetString("_Email");
            string pwd = HttpContext.Session.GetString("_Password");
            Users u1 = (Users)_context.Users.Where(u => u.Email.Equals(email)).FirstOrDefault();
            int user_number = u1.UserNumber;
            ViewBag.user_photo = user_number + ".jpg";
            return View( _context.Users.Where(u => u.Email.Equals(email)).FirstOrDefault());
        }


        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            DataBaseUtils utils = new DataBaseUtils(_context);
            List<string> errors = new List<string>(); // added this just to return something
            string uniqueFilename = null;
            try
            {
                if (file != null)
                {
                    string uploadsFolder = Path.Combine("images");
                    uniqueFilename = Guid.NewGuid().ToString() + "_" + Path.GetFileName(file.FileName);
                    //string filePath = Path.Combine(uploadsFolder, uniqueFilename);
                    string webRootPath = _env.WebRootPath;
                    string filePath = Path.Combine(webRootPath + "//images", uniqueFilename);
                    FileStream newFile = new FileStream(filePath, FileMode.Create);
                    file.CopyTo(newFile);
                    newFile.Close();
                    utils.import_users_excel(filePath);
                    return Json(new string[] { file.FileName });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error : " + ex);
            }
            return Json(new string[] { "no file lol" });
        }

        [HttpPost]
        public async Task<IActionResult> UploadAdvisorFile(IFormFile file)
        {
            DataBaseUtils utils = new DataBaseUtils(_context);
            List<string> errors = new List<string>(); // added this just to return something
            string uniqueFilename = null;
            try
            {
                if (file != null)
                {
                    string uploadsFolder = Path.Combine("images");
                    uniqueFilename = Guid.NewGuid().ToString() + "_" + Path.GetFileName(file.FileName);
                    //string filePath = Path.Combine(uploadsFolder, uniqueFilename);
                    string webRootPath = _env.WebRootPath;
                    string filePath = Path.Combine(webRootPath + "//images", uniqueFilename);
                    FileStream newFile = new FileStream(filePath, FileMode.Create);
                    file.CopyTo(newFile);
                    newFile.Close();
                    utils.import_usersAdvisor_excel(filePath);
                    return Json(new string[] { file.FileName });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error : " + ex);
            }
            return Json(new string[] { "no file lol" });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int UserNumber, [Bind("UserNumber,Name,UserTypesIdType,Address,Email, Password")] Users user)
        {
            if (UserNumber != user.UserNumber)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERRO : " + ex);
                }
                return RedirectToAction("Perfil");
            }
            return RedirectToAction("Perfil");
        }

        // POST: Users/Delete/5
        /** Optimus Prime needed
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        */
        public async Task<IActionResult> Delete(int id)
        {
            var student = await _context.Users.FindAsync(id);
            _context.Users.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction("Students");
        }
        
    }
}
