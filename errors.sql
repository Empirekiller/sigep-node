-- ------------------ErrorLogMessage------------------
USE SWDB;
GO
/****** Object:  StoredProcedure [dbo].[sp_ErrorLogMessage]    Script Date: 15/11/2018 10:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER Procedure [SWS].[sp_ErrorLogMessage]
	(@ErrorCode INT, @Line INT)
AS
BEGIN
	Declare @ErrorId nvarchar(10)
	--Switch
	Select @ErrorId = CASE
		WHEN @ErrorCode=515  THEN 'NullParam'
		WHEN @ErrorCode=547  THEN 'NoNExist'
		WHEN @ErrorCode=2627 THEN 'KeyExists'
		WHEN @ErrorCode=-404 THEN 'NoMatch'
		WHEN @ErrorCode=-400 THEN 'ResetPw'
		WHEN @ErrorCode=-111 THEN 'CartActive'
		WHEN @ErrorCode=-5 THEN 'NoNCur'
		WHEN @ErrorCode=-7 THEN 'Sold'
		WHEN @ErrorCode=-10 THEN 'AlreadyDon'
		WHEN @ErrorCode=-410 THEN 'DateInval'
		ELSE 'Unknown'
	END

	/*PRINT(@ErrorId)*/
	--End Switch
	INSERT INTO [SWS].[ErrorLog] (ErrorCode, [User])
	VALUES (@ErrorId, CURRENT_USER);
	
	
	Declare @Message nvarchar(50)
	SET @Message ='Erro na linha ' + CAST(@Line AS nvarchar(5)) + ': '
	Select @Message += ErrorMessage FROM ErrorMessage WHERE ErrorCode=@ErrorId
	PRINT(@Message)
END


-- CREATES

Create table ErrorMessage (
	ErrorCode nvarchar(10) not null,
	ErrorMessage varchar(50) not null,
	PRIMARY KEY (ErrorCode)
);

Create table ErrorLog(
	ErrorLogKey INT identity(1,1) not null,
	ErrorTimeStamp DATETIME DEFAULT CURRENT_TIMESTAMP not null,
	ErrorCode nvarchar(10) not null,
	[User] varchar(130) not null,
	PRIMARY KEY (ErrorLogKey),
	Foreign Key (ErrorCode) REFERENCES ErrorMessage (ErrorCode)
);

INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('NullParam', 'O parametro enviado e Invalido (nulo)!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('KeyExists', 'Chave primária já existente.');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('Unknown', 'Erro Desconhecido!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('DateInval', 'Data inserida e invalida!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('NoNExist', 'A chave inserida nao existe!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('ProdState', 'Produto indisponivel!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('NoMatch', 'A combinacao usada e incorreta!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('ResetPw', 'Reset de Password!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) Values ('NoReg', 'Não existem registos com os parâmetros fornecidos!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) VALUES ('CartActive', 'Existe um carrinho ativo finalize a sua compra');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) VALUES ('NoNCur','Produto inválido ou Produto não disponivel!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) VALUES ('Sold','Venda efetuada impossivel eliminar produtos!');
INSERT INTO ErrorMessage (ErrorCode, ErrorMessage) VALUES ('AlreadyDon','Encomenda já tinha sido efetuada!');
